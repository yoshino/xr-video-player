/* pipewire.h
 *
 * Copyright 2020 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "gl-egl-common.h"

typedef struct _pipewire_data pipewire_data;

void *pipewire_create(int pipewire_fd, int pipewire_node, struct gl_egl_data *egl_data);
void pipewire_destroy(pipewire_data *pw_data);

void pipewire_show(pipewire_data *pw_data);
void pipewire_hide(pipewire_data *pw_data);
uint32_t pipewire_get_width(pipewire_data *pw_data);
uint32_t pipewire_get_height(pipewire_data *pw_data);
GLuint pipewire_get_texture(pipewire_data *pw_data);
void *pipewire_get_cursor_data(pipewire_data *pw_data);

void pipewire_set_cursor_visible(pipewire_data *pw_data, bool cursor_visible);
