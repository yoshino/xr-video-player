/* portal.c
 *
 * Copyright 2021 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "portal.h"
#include "pipewire.h"

#include <stdio.h>
#include <string.h>

// #include <util/dstr.h>

#define REQUEST_PATH "/org/freedesktop/portal/desktop/request/%s/vr_video_player_%u"
#define SESSION_PATH "/org/freedesktop/portal/desktop/session/%s/vr_video_player_%u"

static GDBusConnection *connection = NULL;

void *bmemdup(const void *ptr, size_t size)
{
	void *out = malloc(size);
	if (size)
		memcpy(out, ptr, size);

	return out;
}

static void ensure_connection(void)
{
	g_autoptr(GError) error = NULL;
	if (!connection) {
		connection = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &error);

		if (error) {
			printf(
			     "[WARNING][portals] Error retrieving D-Bus connection: %s\n",
			     error->message);
			return;
		}
	}
}

char *get_sender_name(void)
{
	char *sender_name;
	char *aux;

	ensure_connection();

	sender_name = bstrdup(g_dbus_connection_get_unique_name(connection) + 1);

	/* Replace dots by underscores */
	while ((aux = strstr(sender_name, ".")) != NULL)
		*aux = '_';

	return sender_name;
}

GDBusConnection *portal_get_dbus_connection(void)
{
	ensure_connection();
	return connection;
}

void portal_create_request_path(char **out_path, char **out_token)
{
	static uint32_t request_token_count = 0;

	request_token_count++;

	if (out_token) {
		char* str = malloc(sizeof(char) * 512);
		snprintf(str, 512, "vr_video_player_%u", request_token_count);
		*out_token = str;
	}

	if (out_path) {
		char *sender_name;
		char* str = malloc(sizeof(char) * 512);

		sender_name = get_sender_name();

		snprintf(str, 512, REQUEST_PATH, sender_name,
			    request_token_count);
		*out_path = str;

		free(sender_name);
	}
}

void portal_create_session_path(char **out_path, char **out_token)
{
	static uint32_t session_token_count = 0;

	session_token_count++;

	if (out_token) {
		char* str = malloc(sizeof(char) * 512);
		snprintf(str, 512, "vr_video_player_%u", session_token_count);
		*out_token = str;
	}

	if (out_path) {
		char *sender_name;
		char* str = malloc(sizeof(char) * 512);

		sender_name = get_sender_name();

		snprintf(str, 512, SESSION_PATH, sender_name,
			    session_token_count);
		*out_path = str;

		free(sender_name);
	}
}
